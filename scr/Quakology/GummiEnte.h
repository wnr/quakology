#ifndef QUAKOLOGY_GUMMI_ENTE_H
#define QUAKOLOGY_GUMMI_ENTE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Quakfaehig.h"
#include "SenderRing.h"
#include "Beobachter.h"


//namespace Quakology
//{
/**
 *  GummiEnte, ist quakfaehig. 
 *   
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class GummiEnte : public Quakfaehig
{
private:
//    std::auto_ptr <SenderRing> _senderRing;
    SenderRing* _senderRing;
    
    GummiEnte( const GummiEnte&);
    void operator=(const GummiEnte&);

public:
	/**
	 *  Konstruktor 
	 * 
	 */
	GummiEnte() : _senderRing ( new SenderRing(this))
    {
    };

	/**
	 *  Benachrichtigt die Beobachter, wird nach dem Quaken aufgerufen. 
	 * 
	 */
	void benachrichtigeBeobachtende() const;

	/**
	 *  Gibt den Tierlaut aus und benachrichtigt die Beobachter. 
	 * 
	 */
	void quaken() const;

	/**
	 *  Fuegt dem Senderring einen Beobachter hinzu. 
	 * 
	 */
	void registriereBeobachter(Beobachter* beobachter);

	/**
	 *  Gibt den Tiertyp zurueck. 
	 * 
	 */
    std::string toString() const;

};

//}  // namespace Aufgabe1
#endif
