#ifndef QUAKOLOGY_GANS_ADAPTER_H
#define QUAKOLOGY_GANS_ADAPTER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Gans.h"
#include "Quakfaehig.h"
#include "SenderRing.h"
#include "Beobachter.h"


//namespace Quakology
//{
/**
 *  Adapter Pattern, damit die Gans wie ein Quakfaehig-Objekt behandelt werden kann. 
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class GansAdapter : public Quakfaehig
{
private:
    //    std::auto_ptr <SenderRing> _senderRing;
    SenderRing* _senderRing;
//    std::auto_ptr< Gans > _gans;
    Gans* _gans;
    
    GansAdapter( const GansAdapter& ); // Disable copy constructor
    void operator=( const GansAdapter& ); // Disable assignment operator

protected:

public:
	/**
	 *  Konstruktor 
	 *  @param gans - Gans-Objekt, das quakfaehig behandelbar sein soll. 
	 * 
	 */
	explicit GansAdapter( Gans* gans ) :_gans( gans ), _senderRing( new SenderRing( this ) ) {
        assert( gans );
	}

	/**
	 *  Tiergerausch ausgeben und Beobachter benachrichtigen. 
	 * 
	 */
	void quaken() const;

	/**
	 *  Fuegt dem Senderring einen neuen Beobachter hinzu. 
	 *  @param beobachter - Zu registrierender Beobachter 
	 * 
	 */
	void registriereBeobachter(Beobachter* beobachter);

	/**
	 *  Benachrichtigt die Beobachter, wird nach dem quaken aufgerufen. 
	 * 
	 */
	void benachrichtigeBeobachtende() const;

	/**
	 *  Gibt den Tiertyp zurueck. 
	 * 
	 */
    std::string toString() const;

};

//}  // namespace Aufgabe1
#endif
