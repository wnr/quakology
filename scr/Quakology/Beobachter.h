#ifndef QUAKOLOGY_BEOBACHTER_H
#define QUAKOLOGY_BEOBACHTER_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>


//namespace Quakology
//{
/**
 *  Observer Pattern
 *  @author Christoph Pernold, Alexander Seik
 *  @version 20.9
 *
 */
class QuakBeobachtungsSubjekt;
class Beobachter
{
    public:
	/**
	 *  Beobachter beobachtet ein quack-Ereignis und gibt dieses aus.
	 *  @param ente - quakende Ente
	 *
	 */
	virtual void aktualiesieren(QuakBeobachtungsSubjekt* ente)=0;

	//virtual ~Beobachter() = 0;
};

//}  // namespace Aufgabe1
#endif
