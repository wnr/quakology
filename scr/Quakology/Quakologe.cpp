#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Quakologe.h"
#include "Quakfaehig.h"

//namespace Quakology
//{

void Quakologe::aktualiesieren(QuakBeobachtungsSubjekt* ente)
{
    std::cout << "Quakologe: " << dynamic_cast< Quakfaehig* >(ente)->toString() << " hat gerade gequakt." << std::endl;
}

std::string Quakologe::toString()
{
	return "Quakologe";
}
//}  // namespace Aufgabe1
