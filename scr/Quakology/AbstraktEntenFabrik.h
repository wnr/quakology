#ifndef QUAKOLOGY_ABSTRAKT_ENTEN_FABRIK_H
#define QUAKOLOGY_ABSTRAKT_ENTEN_FABRIK_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Quakfaehig.h"

//namespace Quakology
//{
/**
 *  Diese abstrakte Klasse entspricht den AbstractFactory Design Pattern 
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class AbstraktEntenFabrik
{
public:
//    virtual ~AbstraktEntenFabrik() = 0;
    
	/**
	 *  Erzeugt ein StockEnten-Objekt. 
	 *  @return Quakfaehig stockente 
	 * 
	 */
	virtual Quakfaehig* erzeugeStockEnte() const=0;

	/**
	 *  Erzeugt ein MoorEnte-Objekt. 
	 *  @return Quakfaehig moorente 
	 * 
	 */
	virtual Quakfaehig* erzeugeMoorEnte() const=0;

	/**
	 *  Erzeugt ein LockPfeife-Objekt. 
	 *  @return Quakfaehig lockpfeife 
	 * 
	 */
	virtual Quakfaehig* erzeugeLockPfeife() const=0;

	/**
	 *  Erzeugt ein GummiEnte-Objekt. 
	 *  @return Quakfaehig gummiente 
	 * 
	 */
	virtual Quakfaehig* erzeugeGummiEnte() const=0;

};

//}  // namespace Aufgabe1
#endif
