#ifndef QUAKOLOGY_LOCK_PFEIFE_H
#define QUAKOLOGY_LOCK_PFEIFE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Quakfaehig.h"
#include "SenderRing.h"
#include "Beobachter.h"


//namespace Quakology
//{
/**
 *  LockPfeife, ist quakfaehig. 
 *   
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class LockPfeife : public Quakfaehig
{
private:
    //    std::auto_ptr <SenderRing> _senderRing;
    SenderRing* _senderRing;
    
    LockPfeife( const LockPfeife&);
    void operator=(const LockPfeife&);

public:
	/**
	 *  Konstruktor 
	 * 
	 */
	LockPfeife() : _senderRing ( new SenderRing(this))
    {
    };

	/**
	 *  Benachrichtigt die Beobachter, wird nach dem Quaken aufgerufen. 
	 * 
	 */
	void benachrichtigeBeobachtende() const;

	/**
	 *  Gibt den Tierlaut aus und benachrichtigt die Beobachter. 
	 * 
	 */
	void quaken() const;

	/**
	 *  Fuegt dem Senderring einen Beobachter hinzu. 
	 * 
	 */
	void registriereBeobachter(Beobachter* beobachter);

	/**
	 *  Gibt den Tiertyp zurueck. 
	 * 
	 */
	std::string toString() const;

};

//}  // namespace Aufgabe1
#endif
