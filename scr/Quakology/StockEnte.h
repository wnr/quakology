#ifndef QUAKOLOGY_STOCK_ENTE_H
#define QUAKOLOGY_STOCK_ENTE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Quakfaehig.h"
#include "SenderRing.h"
#include "Beobachter.h"


//namespace Quakology
//{
/**
 *  StockEnte, ist quakfaehig. 
 *   
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class StockEnte : public Quakfaehig
{
private:
//    std::auto_ptr<SenderRing> _senderRing;
    SenderRing* _senderRing;
    StockEnte( const StockEnte&);
    void operator=(const StockEnte&);

public:
	/**
	 *  Konstruktor 
	 * 
	 */
	StockEnte() : _senderRing ( new SenderRing(this))
    {
    };

	/**
	 *  Benachrichtigt die Beobachter, wird nach dem Quaken aufgerufen. 
	 * 
	 */
	void benachrichtigeBeobachtende() const;

	/**
	 *  Gibt den Tierlaut aus und benachrichtigt die Beobachter. 
	 * 
	 */
	void quaken() const;

	/**
	 *  Fuegt dem Senderring einen Beobachter hinzu. 
	 * 
	 */
	void registriereBeobachter(Beobachter* beobachter);

	/**
	 *  Gibt den Tiertyp zurueck. 
	 * 
	 */
    std::string toString() const;

};

//}  // namespace Aufgabe1
#endif
