#ifndef QUAKOLOGY_GANS_H
#define QUAKOLOGY_GANS_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

//namespace Quakology
//{
/**
 *  Gans kann schnattern, verfuegt nicht ueber die quackfaehigen-Methoden, 
 *  deshalb gibt es den Gansadapter. (Adapter Pattern) 
 *   
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class Gans
{
public:
	/**
	 *  Gibt "Schnattern" aus. 
	 * 
	 */
	void schnattern();

	/**
	 *  Gibt den Tiertyp zurueck. 
	 * 
	 */
    std::string toString() const;

};

//}  // namespace Aufgabe1
#endif
