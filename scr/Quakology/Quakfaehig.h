#ifndef QUAKOLOGY_QUAKFAEHIG_H
#define QUAKOLOGY_QUAKFAEHIG_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "QuakBeobachtungsSubjekt.h"
#include "Beobachter.h"


//namespace Quakology
//{
/**
 *  Interface fuer Quakfaehige Objekte. 
 *   
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class Quakfaehig : public QuakBeobachtungsSubjekt
{
public:
	/**
	 *  Gibt den Tierlaut aus. 
	 * 
	 */
	virtual void quaken() const=0;

	/**
	 *  Fuegt dem Senderring einen Beobachter hinzu. 
	 * 
	 */
	virtual void registriereBeobachter(Beobachter* beobachter)=0;

	/**
	 *  Benachrichtigt die Beobachter aus dem Senderring. 
	 * 
	 */
	virtual void benachrichtigeBeobachtende()const=0;

	/**
	 *  Gibt den Tiertyp zurueck. 
	 *  @return String 
	 * 
	 */
	virtual std::string toString()const=0;

};

//}  // namespace Aufgabe1
#endif
