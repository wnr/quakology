#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "StockEnte.h"

//namespace Quakology
//{

void StockEnte::benachrichtigeBeobachtende() const
{
    _senderRing->benachrichtigeBeobachtende();
}

void StockEnte::quaken() const
{
    std::cout << "Quak" << std::endl;
    benachrichtigeBeobachtende();
}

void StockEnte::registriereBeobachter(Beobachter* beobachter)
{
    _senderRing->registriereBeobachter(beobachter);
}

std::string StockEnte::toString() const
{
	return "Stockente";
}
//}  // namespace Aufgabe1
