#ifndef QUAKOLOGY_QUAK_BEOBACHTUNGS_SUBJEKT_H
#define QUAKOLOGY_QUAK_BEOBACHTUNGS_SUBJEKT_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Beobachter.h"

//namespace Quakology
//{
    /**
     *  QuackBeobachtungsSubjekt, wird einem SenderRing hinzugefuegt.
     *
     *  @author Christoph Pernold, Alexander Seik
     *  @version 20.9
     *
     */
class QuakBeobachtungsSubjekt
{
public:
    
//    virtual ~QuakBeobachtungsSubjekt() = 0;

    
    /**
     *  Fuegt dem Senderring einen Beobachter hinzu
     *  @param beobachter - wird dem Senderring hinzugefuegt
     *
     */
    virtual void registriereBeobachter(Beobachter* beobachter)=0;
        
    /**
     *  Benachrichtigt die Beobachter aus dem Senderring
     * 
     */
    virtual void benachrichtigeBeobachtende() const=0;
    
};
//}  // namespace Aufgabe1
#endif

