#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "QuakZaehler.h"

//namespace Quakology
//{

void QuakZaehler::quaken() const
{
    ente->quaken();
    QuakZaehler::anzahlDerQuaks++;
}

int QuakZaehler::getQuaks()
{
	return anzahlDerQuaks;
}

void QuakZaehler::registriereBeobachter(Beobachter* beobachter)
{
    ente->registriereBeobachter(beobachter);
}

void QuakZaehler::benachrichtigeBeobachtende() const
{
    ente->benachrichtigeBeobachtende();
}

std::string QuakZaehler::toString() const
{
	return ente->toString();
}

//}  // namespace Aufgabe1
