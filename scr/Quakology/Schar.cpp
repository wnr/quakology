#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Schar.h"

//namespace Quakology
//{

Schar::Schar()
{
}
    
void Schar::hinzufuegen(Quakfaehig* ente)
{
    _quakende.push_back(ente);
}

void Schar::quaken() const
{
    std::vector<Quakfaehig*>::iterator iterator=_quakende.begin();
    for (; _quakende.end()!= iterator; ++iterator) {
        Quakfaehig* quaker = *iterator;
        quaker->quaken();
//        delete quaker;
    }
}

void Schar::registriereBeobachter(Beobachter* beobachter)
{
    std::vector<Quakfaehig*>::iterator iterator=_quakende.begin();
    for (; _quakende.end()!= iterator; ++iterator) {
        Quakfaehig* quaker = *iterator;
        quaker->registriereBeobachter(beobachter);
//        delete quaker;
    }
}

void Schar::benachrichtigeBeobachtende()const
{
}

std::string Schar::toString() const
{
	return "Entenschar";
}
//}  // namespace Aufgabe1
