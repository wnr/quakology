#ifndef QUAKOLOGY_QUAK_ZAEHLER_H
#define QUAKOLOGY_QUAK_ZAEHLER_H

#include <stdlib.h>
#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Quakfaehig.h"
#include "Beobachter.h"


//namespace Quakology
//{
/**
 *  Diese Klasse zaehlt die Quaks
 *  @author Christoph Pernold, Alexander Seik
 *  @version 20.9
 *
 */
class QuakZaehler : public Quakfaehig
{
private:
    static int anzahlDerQuaks;
    
    //    std::auto_ptr<Quakfaehig> _ente;
    Quakfaehig* ente;
    
    QuakZaehler(const QuakZaehler&);
    void operator=(const QuakZaehler&);
    
public:
    
	/**
	 *  Konstruktor
	 *  @param ente
	 *
	 */
	explicit QuakZaehler ( Quakfaehig* ente ) : ente( ente )
    {
        assert( ente );
	}
    
	/**
	 *  Zahler wird erhoeht
	 *  und quaken wird ausgef?hrt
	 *
	 */
	void quaken() const;
    
	/**
	 *  Gibt die anzahl der Quaks zur?ck
	 *  @return
	 *
	 */
	static int getQuaks();
    
	/**
	 *  Beobachter werden registriert
	 *  @param beobachter
	 *
	 */
	void registriereBeobachter(Beobachter* beobachter);
    
	/**
	 *  Beobachter wird benachrichtigt
	 *
	 */
	void benachrichtigeBeobachtende() const;
    
	/**
	 *  Quaks werden ausgegben
	 *  @return String
	 *
	 */
    std::string toString() const;
    
};

//}  // namespace Aufgabe1
#endif
