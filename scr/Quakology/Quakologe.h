#ifndef QUAKOLOGY_QUAKOLOGE_H
#define QUAKOLOGY_QUAKOLOGE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Beobachter.h"
#include "QuakBeobachtungsSubjekt.h"


//namespace Quakology
//{
/**
 *  Quackologe faellt unters Observer Pattern 
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class Quakologe : public Beobachter
{
    
public:
	/**
	 *  Quakologe gibt die akutalisierung aus 
	 *  z.B. Quakologe: Moorente hat gerade gequakt 
	 *  @param ente 
	 * 
	 */
	void aktualiesieren(QuakBeobachtungsSubjekt* ente);

	/**
	 *  Gibt die aktuelle Ente als String zur?ck 
	 *  @return String 
	 * 
	 */
    std::string toString();

};

//}  // namespace Aufgabe1
#endif
