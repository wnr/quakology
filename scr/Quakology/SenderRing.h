#ifndef QUAKOLOGY_SENDER_RING_H
#define QUAKOLOGY_SENDER_RING_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "QuakBeobachtungsSubjekt.h"


//namespace Quakology
//{
/**
 *  Diese Klasse erzeugt Quakfaehig Objekte (AbstractFactory Pattern) inkl. Zaehler 
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class SenderRing : public QuakBeobachtungsSubjekt
{
private:
	mutable std::vector<Beobachter*> _beobachtende;
	QuakBeobachtungsSubjekt* _ente;
    
    SenderRing( const SenderRing& ); // Disable copy constructor
    void operator=( const SenderRing& );

public:
	/**
	 *  Konstruktor 
	 *  @param ente - Objekt, dem der Beobachter hinzugefuegt wurde 
	 * 
	 */
     explicit SenderRing( QuakBeobachtungsSubjekt* ente ) :_ente( ente ) {
            assert( ente );
     }

	/**
	 *  Fuegt dem der ArrayList den uebergebenen Beobachter hinzu. 
	 * 
	 */
	void registriereBeobachter(Beobachter* beobachter);

	/**
	 *  Benachrichtigt die Beobachter aus der ArrayList. 
	 * 
	 */
	void benachrichtigeBeobachtende() const;

	/**
	 *  Gibt den Iterator der ArrayList zurueck, dadurch kann man die Beobachter auslesen. 
	 *  @return Iterator<Beobachter> 
	 * 
	 */
    std::vector<Beobachter*> getBeobachtende();

};

//}  // namespace Aufgabe1
#endif
