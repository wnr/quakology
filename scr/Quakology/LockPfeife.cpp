#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "LockPfeife.h"

//namespace Quakology
//{

void LockPfeife::benachrichtigeBeobachtende() const
{
    _senderRing->benachrichtigeBeobachtende();
}

void LockPfeife::quaken() const
{
    std::cout << "Kwaak" << std::endl;
    benachrichtigeBeobachtende();
}

void LockPfeife::registriereBeobachter(Beobachter* beobachter)
{
    _senderRing->registriereBeobachter(beobachter);    
}

std::string LockPfeife::toString() const
{
    return "Lockpfeife";
}
//}  // namespace Aufgabe1
