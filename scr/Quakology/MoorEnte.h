#ifndef QUAKOLOGY_MOOR_ENTE_H
#define QUAKOLOGY_MOOR_ENTE_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Quakfaehig.h"
#include "SenderRing.h"
#include "Beobachter.h"



//namespace Quakology
//{
/**
 *  MoorEnte, ist quakfaehig. 
 *   
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class MoorEnte : public Quakfaehig
{
private:
    //    std::auto_ptr <SenderRing> _senderRing;
    SenderRing* _senderRing;
    
    MoorEnte( const MoorEnte&);
    void operator=(const MoorEnte&);
    
public:
	/**
	 *  Konstruktor 
	 * 
	 */
	MoorEnte() : _senderRing ( new SenderRing(this))
    {
    };

	/**
	 *  Benachrichtigt die Beobachter, wird nach dem Quaken aufgerufen. 
	 * 
	 */
	void benachrichtigeBeobachtende() const;

	/**
	 *  Gibt den Tierlaut aus und benachrichtigt die Beobachter. 
	 * 
	 */
	void quaken() const;

	/**
	 *  Fuegt dem Senderring einen Beobachter hinzu. 
	 * 
	 */
	void registriereBeobachter(Beobachter* beobachter);

	/**
	 *  Gibt den Tiertyp zurueck. 
	 * 
	 */
	std::string toString() const;

};

//}  // namespace Aufgabe1
#endif
