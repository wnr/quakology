#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "EntenSimulator.h"


//namespace Quakology
//{

//int QuakZaehler::_anzahlDerQuaks = 0;

void simulieren(const Quakfaehig* ente)
{
    assert(ente);
    ente->quaken();
}

int main(int argc, char* argv[]) {
    std::cout << "++++++++++++\n++++++++++++++++++\n++++++++++++++++++++++\n++++++++++++++++++++++++ + ++++\n++++++++++++++++++++++++ +++ ++++++\n++++++++++++++++++++++++++++++++++++\n++++++++++++++++++++++++++++++++++++\n:::::::::,a@@a,:::::,a@a,++++++++++.\n.ooOOOOOOOOOOo@@@@@@oOoOo@@@@@,++++++++/:.\no OOOOOOOOOOOOo@@@@@@@@@oOOo@@@@@@,++++++/:::\no oOOOOOOOOOOOOOo@@@@@@@@@@@oOo@@@@@@a   :::::::\noOoOOOOOOOOOOOOOOo@@@@@@@@@@@oOo@@@@@@@   :::::::\noOOOOOOOOOOOOOOOOo@@@@@@@@@@@@oOo@@@@@@@   ::: ::\noOOOOOOOOOOOOOOOOo`   @@@@@@@@oOo   @@@@   :\noOOOOOOO%%%%%OOOOo    @@@@@@@@oOo   @@@a\noOOOO%%%.%%%OOOo.  ,@@@@@@@oOOo. ,@@@ \noOOO%%%.%%%%%OOOoa@@@@@@@@oOOOo@@@@@\nOOO%%%.%%%%%%OOo@@@@@@@@oOOOOOo@@@         . %%%%.\nOOO%%.%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\nOO%%.%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\nO%%.....  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n%%.............%%%%%%%%%%%%%%%%%%%%%%%\n%%............`%%\n%%............%%\n%%...........%%\n%%%%%%%%%%%%%\n`%%%%%%%%%\n";

    AbstraktEntenFabrik* entenFabrik( new ZaehlendeEntenFabrik() );

    Quakfaehig* moorEnte = entenFabrik->erzeugeMoorEnte();
    Quakfaehig* lockPfeife = entenFabrik->erzeugeLockPfeife();
    Quakfaehig* gummiEnte = entenFabrik->erzeugeGummiEnte();
    Quakfaehig* gansEnte = new GansAdapter(new Gans());

    Schar* entenSchar(new Schar());

    entenSchar->hinzufuegen(lockPfeife);
    entenSchar->hinzufuegen(moorEnte);
    entenSchar->hinzufuegen(gummiEnte);
    entenSchar->hinzufuegen(gansEnte);

    Schar* stockEntenSchar(new Schar());

    Quakfaehig* stockEnte1 = entenFabrik->erzeugeStockEnte();
    Quakfaehig* stockEnte2 = entenFabrik->erzeugeStockEnte();
    Quakfaehig* stockEnte3 = entenFabrik->erzeugeStockEnte();
    Quakfaehig* stockEnte4 = entenFabrik->erzeugeStockEnte();

    stockEntenSchar->hinzufuegen(stockEnte1);
    stockEntenSchar->hinzufuegen(stockEnte2);
    stockEntenSchar->hinzufuegen(stockEnte3);
    stockEntenSchar->hinzufuegen(stockEnte4);

    entenSchar->hinzufuegen(stockEntenSchar);

    std::cout << "\nEntensimulator: mit Observer" << std::endl;

    Quakologe* quakologe = new Quakologe();
    entenSchar->registriereBeobachter(quakologe);

    simulieren(entenSchar);

    int count = QuakZaehler::getQuaks();

    std::cout << "\nDie Enten haben " << count << "-mal gequakt.";

    return 0;
}

//}  // namespace Aufgabe1