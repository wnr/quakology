#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "GansAdapter.h"

//namespace Quakology
//{


void GansAdapter::quaken() const
{
    _gans->schnattern();
    benachrichtigeBeobachtende();
}

void GansAdapter::registriereBeobachter(Beobachter* beobachter)
{
    _senderRing->registriereBeobachter(beobachter);
}

void GansAdapter::benachrichtigeBeobachtende() const
{
    _senderRing->benachrichtigeBeobachtende();
}

std::string GansAdapter::toString() const
{
    return "sich als Ente ausgebende Gans";
}
//}  // namespace Aufgabe1
