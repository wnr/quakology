#ifndef QUAKOLOGY_ENTENFABRIK_H
#define QUAKOLOGY_ENTENFABRIK_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "AbstraktEntenFabrik.h"
#include "Quakfaehig.h"


//namespace Quakology
//{
/**
 *  Diese Klasse erzeugt Quakfaehig Objekte (AbstractFactory Pattern) 
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class Entenfabrik : public AbstraktEntenFabrik
{
public:
	/**
	 *  Gibt ein Objekt der Stockente zur?ck 
	 * 
	 */
	Quakfaehig* erzeugeStockEnte() const=0;

	/**
	 *  Gibt ein Objekt der MoorEnte zur?ck  
	 * 
	 */
	Quakfaehig* erzeugeMoorEnte() const=0;

	/**
	 *  Gibt ein Objekt der LockPfeife zur?ck 
	 * 
	 */
	Quakfaehig* erzeugeLockPfeife() const=0;

	/**
	 *  Gibt ein Objekt der GummiEnte zur?ck 
	 * 
	 */
	Quakfaehig* erzeugeGummiEnte() const=0;

};

//}  // namespace Aufgabe1
#endif
