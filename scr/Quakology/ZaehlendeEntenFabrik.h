#ifndef QUAKOLOGY_ZAEHLENDE_ENTEN_FABRIK_H
#define QUAKOLOGY_ZAEHLENDE_ENTEN_FABRIK_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "AbstraktEntenFabrik.h"
#include "Quakfaehig.h"


//namespace Quakology
//{
/**
 *  Diese Klasse erzeugt Quakfaehig Objekte (AbstractFactory Pattern) inkl. Zaehler 
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class ZaehlendeEntenFabrik : public AbstraktEntenFabrik
{
public:
	/**
	 *  Gibt ein Objekt der Stockente zurueck welches im Quakzaehler registriert wurde
	 * 
	 */
	Quakfaehig* erzeugeStockEnte() const;

	/**
	 *  Gibt ein Objekt der Moorente zurueck welches im Quakzaehler registriert wurde 
	 * 
	 */
	Quakfaehig* erzeugeMoorEnte() const;

	/**
	 *  Gibt ein Objekt der Lockpfeife zurueck welches im Quakzaehler registriert wurde 
	 * 
	 */
	Quakfaehig* erzeugeLockPfeife() const;

	/**
	 *  Gibt ein Objekt der Gummiente zurueck welches im Quakzaehler registriert wurde 
	 * 
	 */
	Quakfaehig* erzeugeGummiEnte() const;

};

//}  // namespace Aufgabe1
#endif
