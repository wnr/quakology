#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Entenfabrik.h"
#include "StockEnte.h"
#include "MoorEnte.h"
#include "LockPfeife.h"
#include "GummiEnte.h"

//namespace Quakology
//{

Quakfaehig* Entenfabrik::erzeugeStockEnte() const
{
	return new StockEnte();
}

Quakfaehig* Entenfabrik::erzeugeMoorEnte() const
{
	return new MoorEnte();
}

Quakfaehig* Entenfabrik::erzeugeLockPfeife() const
{
	return new LockPfeife();
}

Quakfaehig* Entenfabrik::erzeugeGummiEnte() const
{
	return new GummiEnte();
}
//}  // namespace Aufgabe1
