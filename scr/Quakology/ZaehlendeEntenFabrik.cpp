#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "ZaehlendeEntenFabrik.h"
#include "StockEnte.h"
#include "MoorEnte.h"
#include "LockPfeife.h"
#include "GummiEnte.h"
#include "QuakZaehler.h"

//namespace Quakology
//{

Quakfaehig* ZaehlendeEntenFabrik::erzeugeStockEnte() const
{
	return new QuakZaehler(new StockEnte());
}

Quakfaehig* ZaehlendeEntenFabrik::erzeugeMoorEnte() const
{
	return new QuakZaehler(new MoorEnte());
}

Quakfaehig* ZaehlendeEntenFabrik::erzeugeLockPfeife() const
{
	return new QuakZaehler(new LockPfeife());
}

Quakfaehig* ZaehlendeEntenFabrik::erzeugeGummiEnte() const
{
	return new QuakZaehler(new GummiEnte());
}
//}  // namespace Aufgabe1
