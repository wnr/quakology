#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "MoorEnte.h"

//namespace Quakology
//{

void MoorEnte::benachrichtigeBeobachtende() const
{
    _senderRing->benachrichtigeBeobachtende();
}

void MoorEnte::quaken() const
{
    std::cout << "Kwaak" << std::endl;
    benachrichtigeBeobachtende();
}

void MoorEnte::registriereBeobachter(Beobachter* beobachter)
{
    _senderRing->registriereBeobachter(beobachter);
}

std::string MoorEnte::toString() const
{
    return "Lockpfeife";
}
//}  // namespace Aufgabe1


