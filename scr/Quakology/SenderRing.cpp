#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "SenderRing.h"

//namespace Quakology
//{


void SenderRing::registriereBeobachter(Beobachter* beobachter)
{
    _beobachtende.push_back(beobachter);
}

void SenderRing::benachrichtigeBeobachtende() const
{
    std::vector< Beobachter* >::iterator iterator = _beobachtende.begin();
    while( iterator != _beobachtende.end() ) {
        Beobachter* beobachter = *iterator++;
        beobachter->aktualiesieren( _ente );
    }
}

std::vector<Beobachter*> SenderRing::getBeobachtende()
{
	return _beobachtende;
}
//}  // namespace Aufgabe1
