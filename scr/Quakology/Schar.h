#ifndef QUAKOLOGY_SCHAR_H
#define QUAKOLOGY_SCHAR_H

#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Quakfaehig.h"
#include "Beobachter.h"


//namespace Quakology
//{
/**
 *  Diese Klasse verwaltet mehrere Quackfaehig-Objekte 
 *  @author Christoph Pernold, Alexander Seik 
 *  @version 20.9 
 * 
 */
class Schar : public Quakfaehig
{
private:
	mutable std::vector<Quakfaehig*> _quakende;
    Schar(const Schar& );
    void operator=(const Schar&);

public:
    
    Schar();

	/**
	 *  Fuegt ein Objekt der Schar hinzu 
	 *  @param ente 
	 * 
	 */
	void hinzufuegen(Quakfaehig* ente);

	/**
	 *  Laesst alle Quakfaehig quacken 
	 * 
	 */
	void quaken() const;

	/**
	 *  Beobachter wird allen Quackfaehig in der Schar hinzugefuegt 
	 *  @param beobachter 
	 * 
	 */
	void registriereBeobachter(Beobachter* beobachter);

	/**
	 *  Alle beobachter werden benachrichtigt 
	 * 
	 */
	void benachrichtigeBeobachtende()const;

	/**
	 *  Alle Enten werden ausgegebn 
	 *  @return String 
	 * 
	 */
    std::string toString()const;

};

//}  // namespace Aufgabe1
#endif
