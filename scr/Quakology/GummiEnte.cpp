#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "GummiEnte.h"

//namespace Quakology
//{

void GummiEnte::benachrichtigeBeobachtende() const
{
    _senderRing->benachrichtigeBeobachtende();
}

void GummiEnte::quaken() const
{
    std::cout << "Quietsch" << std::endl;
    benachrichtigeBeobachtende();
}

void GummiEnte::registriereBeobachter(Beobachter* beobachter)
{
    _senderRing->registriereBeobachter(beobachter);
}

std::string GummiEnte::toString() const
{
	return "Gummiente";
}
//}  // namespace Aufgabe1
